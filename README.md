### How to use:

1) Open ClickerHeroes and Launch this bot

2) Hover over the monster with your mouse

3) Press PageUP to start/stop the bot

4) PageDOWN does a single mouse click whenever you hover over the ClickerHeroes window

**The bot can work IN BACKGROUND while the window is INACTIVE!** I.e. you can keep browsing interwebs and look at those cute kitten pics while farming in ClickerHeroes!